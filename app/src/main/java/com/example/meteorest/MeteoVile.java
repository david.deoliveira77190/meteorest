package com.example.meteorest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MeteoVile extends AppCompatActivity {

    private TextView tvReponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meteo_vile);
        String reponse = this.getIntent().getExtras().getString("Reponse");

        tvReponse = (TextView) this.findViewById(R.id.tvReponse);
        tvReponse.setText(reponse);

    }

    public void onResponse(String response) {
        Log.i("response",""+response);
        // Lancement de la nouvelle page avec transmission des résultats
        Intent intentMeteo = new Intent(this,MeteoVile.class);
        intentMeteo.putExtra("Reponse", response);
        startActivity(intentMeteo);
    }

}