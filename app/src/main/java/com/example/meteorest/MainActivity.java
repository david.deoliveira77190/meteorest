package com.example.meteorest;

import android.content.DialogInterface;
import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.meteorest.repository.VilleRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import static android.provider.Contacts.SettingsColumns.KEY;

public class MainActivity extends AppCompatActivity {

    private VilleRepository vllRepo;
    private Object laVille;
    String URL_METEO = "http://api.openweathermap.org/data/2.5/weather?q=";
    String KEY = "&appid=898abaee532bd0ebf800c05770607972";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDemande();
            }
        });
        // Creation de l’objet Ville lié a l’utilisateur avec passage du contexte
        vllRepo = new VilleRepository(getApplicationContext());
        // Verification de l'existence d'une ville
        if (vllRepo.isVilleConfigured()) {
            laVille = vllRepo.getVille();

        } else {
            Toast.makeText(this, "Pas de ville déjà demandée...", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onDemande() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        final EditText taskEditText = new EditText(this);
        builder.setMessage("Nom de la ville")
                .setTitle("Demande météo")
                .setView(taskEditText)
                .setNegativeButton("Abandon", null);
        builder.setPositiveButton("Recherche", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                laVille = taskEditText.getText().toString();
                // Envoi avec l’URL complète demandée par l’API
                sendRequest(URL_METEO + laVille + KEY);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendRequest(String URL) {
        StringRequest stringRequest = new StringRequest(URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("response",""+response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(0);
                        getParent().finish();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}